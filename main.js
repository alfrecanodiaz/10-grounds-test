const totalWorkingHours = ((15 * 5) * 4) * 9; // 15 hours, 5 days, 4 weeks, 9 months
const units = [];
const failedTubes = [];
let tubesCost = (4 * 4) * 7; // initial value for 4 tubes per 4 units

const minWorkingHour = 100;
const maxWorkingHour = 200;

function init() {
    for (let i = 0; i < 4; i++) {
        const tubes = [];

        for (let x = 0; x < 4; x++) {
            tubes.push({fixedHours: rand()});
        }

        units.push({tubes: tubes, failed: 0});
    }
}

function simulate() {
    for (let i = 0; i < totalWorkingHours; i++) {

        units.forEach(u => {

            u.tubes.forEach(t => {

                if (t.fixedHours > 0)
                    t.fixedHours = t.fixedHours - 1;

                const brokenTubes = u.tubes.filter(e => e.fixedHours === 0);

                if (brokenTubes.length === 2) {

                    brokenTubes.forEach(e => failedTubes.push(e));

                    u.tubes.forEach((b, i) => {
                        u.tubes[i] = {fixedHours: rand()};
                        tubesCost += 7;
                    });
                }
            })
        });
    }
}

function rand() {
    return Math.floor(Math.random() * (maxWorkingHour - minWorkingHour + 1)) + minWorkingHour;
}

init();
simulate();

console.log('total fluorescent tubes broken in 1 year', failedTubes.length);
console.log('total cost per year per classroom', tubesCost);
console.log('total hours simulated', totalWorkingHours);
